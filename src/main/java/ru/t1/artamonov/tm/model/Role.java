package ru.t1.artamonov.tm.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@Table(name = "tm_role")
public class Role {

    @Id
    @Column
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    public Role(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}
