package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.artamonov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.artamonov.tm.api.service.IProjectService;
import ru.t1.artamonov.tm.model.Project;
import ru.t1.artamonov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.artamonov.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndointImpl implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project) {
        projectService.deleteByUserId(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        projectService.deleteByIdAndUserId(UserUtil.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/create")
    public Project create(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.addByUserId(UserUtil.getUserId(), project);
        return project;
    }

    @NotNull
    @Override
    @WebMethod
    @PutMapping("/update")
    public Project update(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.updateByUserId(UserUtil.getUserId(), project);
        return project;
    }

}
