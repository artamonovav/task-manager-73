package ru.t1.artamonov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.artamonov.tm.api.service.IProjectService;
import ru.t1.artamonov.tm.model.CustomUser;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/projects")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("project-list", "projects", projectService.findAllByUserId(user.getUserId()));
    }

}
