package ru.t1.artamonov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    void delete(Project project);

    @WebMethod
    void deleteById(String id);

    @Nullable
    @WebMethod
    Collection<Project> findAll();

    @Nullable
    @WebMethod
    Project findById(String id);

    @NotNull
    @WebMethod
    Project create(Project project);

    @NotNull
    @WebMethod
    Project update(Project project);

}
